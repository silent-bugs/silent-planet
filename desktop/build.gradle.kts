import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(project(":core"))
    implementation(project(":graphic"))
    api("com.badlogicgames.gdx:gdx-backend-lwjgl:${Versions.gdx}")
    api("com.badlogicgames.gdx:gdx-platform:${Versions.gdx}:natives-desktop")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}")
    implementation("com.badlogicgames.gdx:gdx-freetype-platform:${Versions.gdx}:natives-desktop")
}

sourceSets {
    named("main") {
        java.setSrcDirs(listOf("src/"))
        resources.setSrcDirs(listOf("../graphic/assets"))
    }
}

project.extra["mainClassName"] = "com.silentgames.desktop.DesktopLauncher"
project.extra["assetsDir"] = File("../graphic/assets")

tasks.register<JavaExec>("run") {
    dependsOn("classes")
    mainClass.set(project.extra["mainClassName"] as String)
    classpath(project.the<SourceSetContainer>()["main"].runtimeClasspath)
    standardInput = System.`in`
    workingDir = project.extra["assetsDir"] as File
    isIgnoreExitValue = true
}

tasks.register<JavaExec>("debug") {
    dependsOn("classes")
    mainClass.set(project.extra["mainClassName"] as String)
    classpath(project.the<SourceSetContainer>()["main"].runtimeClasspath)
    standardInput = System.`in`
    workingDir = project.extra["assetsDir"] as File
    isIgnoreExitValue = true
    debug = true
}
tasks.register<Jar>("dist") {
    dependsOn("classes")
    manifest {
        attributes["Main-Class"] = project.extra["mainClassName"]
    }
    from(
        project.the<SourceSetContainer>()["main"].compileClasspath.map {
            if (it.isDirectory) it else zipTree(
                it
            )
        }
    )
    with(tasks["jar"] as CopySpec)
}
