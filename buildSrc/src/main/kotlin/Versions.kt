object Versions {
    const val kotlin = "1.7.10"
    const val gdx = "1.11.0"
    const val roboVM = "2.3.8"
    const val ktx = "1.11.0-rc3"
    const val roboVMGradle = "2.3.8"
    const val gson = "2.8.6"
}
