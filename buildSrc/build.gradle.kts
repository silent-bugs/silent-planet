plugins {
    `kotlin-dsl`
}

kotlin {
    kotlinDslPluginOptions {
        jvmTarget.set("11")
    }
}

repositories {
    mavenCentral()
}
