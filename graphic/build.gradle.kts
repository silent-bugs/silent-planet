plugins {
    id("java-library")
    kotlin("jvm")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation(project(":core"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}")
    implementation("io.github.libktx:ktx-scene2d:${Versions.ktx}")
    implementation("io.github.libktx:ktx-style:${Versions.ktx}")
    api("com.badlogicgames.gdx:gdx:${Versions.gdx}")
    implementation("com.badlogicgames.gdx:gdx-freetype:${Versions.gdx}")
    implementation("com.google.code.gson:gson:${Versions.gson}")
}

sourceSets {
    named("main") {
        java.setSrcDirs(
            listOf(
                "src/"
            )
        )
    }
}
